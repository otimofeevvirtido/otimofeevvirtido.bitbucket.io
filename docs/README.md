## Educaplan VueJS documentation

### Requirements:
* [Symfony](https://symfony.com/download)
* [Composer](https://getcomposer.org/download/)
* [NodeJS](https://nodejs.org/) 

### Setup:
* install npm packages: `npm install`
* install composer packages: `composer install`

### Run:
* build js scripts: `npm run build` || npm watch: `npm run watch`
* start symfony server: `symfony server:start`
