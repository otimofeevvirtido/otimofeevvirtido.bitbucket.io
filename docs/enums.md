# Available enum values
-----

## Desired degrees:

```javascript
[
    "Eidgenössisches Fähigkeitszeugnis EFZ",
    "Eidgenössischer Fachausweis",
    "Eidgenössisches Diplom",
    "Diplom Höhere Fachschule HF",
    "Bachelor Fachhochschule",
    "Bachelor Universität",
    "Master Fachhochschule",
    "Master Universität",
    "Anderes"
]
```

## Id cards:

```javascript
[
    "Schweizer Pass oder Id",
    "Ausweis B EU/EFTA-bürger (Aufenthaltsbewilligung)",
    "Ausweis C EU/EFTA-bürger (Niederlassungsbewilligung)",
    "Ausweis L EU/EFTA-bürger (Kurzaufenthaltsbewilligung)",
    "Ausweis G EU/EFTA (Grenzagängerbewilligung)",
    "Ausweis B Drittstaatsangehörige (Aufenathaltsbewilligung für Drittstaatangehörige - jährliche Erneuerung",
    "Ausweis C Drittstaatsangehörige (Niederlassungsbewilligung für Drittstaatangehörige)",
    "Ausweis L Drittstaatsangehörige (Niederlassungsbewilligung für Drittstaatangehörige)",
    "Ausweis N (Asylsuchender)",
    "Ausweis F (vorläufig Aufgenommen",
    "Ausweis S (Schutzbedürftige - vorläufinger Aufenthalt)" 
]
```

## Information sources:

```javascript
[
    "Internetsuche",
    "Studienberatung",
    "Stipendienberatung",
    "Empfehlung von Kollegen oder Bekannten",
    "Zeitungsartike",
    "Stipendium.ch",
    "Eduwo.ch",
    "Facebook",
    "Flyer",
    "Anderes" 
]
```

## Nationalities: 

```javascript
[
    "Deutschland", 
    "Österreich", 
    "Schweiz" 
]
```

## Genders: 

```javascript
[
    "Männlich", 
    "Weiblich", 
    "Andere"
]
```