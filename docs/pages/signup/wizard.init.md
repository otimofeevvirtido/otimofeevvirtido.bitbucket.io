# Signup init wizard
----

## Forms
![](../../img/component/signup/init_wizard_1.png)
![](../../img/component/signup/init_wizard_2.png)

## API

### Registration init

`[POST] /api/registration` 

Request

```javascript
{
    email: String,
    password: String,
    confirmPassword: String
}
```
Response:

```javascript
{
    email: Boolean,
    password: Boolean,
    email_sent: Boolean
}
```

### Resend confirm email

`[POST] /api/registration/resend`

Requiest:

```javascript
{
    email: String
}
```

Response:

```javascript
{
    email: Boolean,
    password: Boolean,
    email_sent: Boolean
}
```

## Validation

### Email address
* Only email style. (exists @ character and {part1}@{part2}.{domain})
* Maximum 100 characters

### Password
* Minimum 8 characters
* Maximum 100 characters
* Required at least 1 number
* Required at least 1 UPPER
* Required at least 1 lower 

### Confirm password
* Match with password field