# Signup init wizard
----

## Forms
![](../../img/component/signup/confirm_wizard_1.png)
![](../../img/component/signup/confirm_wizard_2.1.png)
![](../../img/component/signup/confirm_wizard_2.2.png)
![](../../img/component/signup/confirm_wizard_3.png)

## API

### Confirm registration

`[POST] NO`

Request:

```javascript 
{
    haveBudget: Boolean,
    docBudget: FileList,
    docCV: FileList,
    description: String,
    gdprAgree: Boolean
}
```

Response: 

```javascript
{
    haveBudget: Boolean,
    docBudget: Boolean,
    docCV: Boolean,
    description: Boolean,
    gdprAgree: Boolean
}
```

### Resend confirm email

`[POST] /api/registration/resend`

Requiest:

```javascript
{
    email: String
}
```

Response:

```javascript
{
    email: Boolean
}
```

## Validation

### Doc budget
* **Required if budget provided**
* extensions: .doc,.docx,.xml,.xls,.xlsx
* formats: application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf

### Doc CV
* **Required if budget provided**
* extensions: .doc,.docx,.xml,.xls,.xlsx
* formats: application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf

### Description
* **Required if budget provided**
* Minimum 100 characters (trim whitespaces)
* Maximum 600 characters 

### GDPR Agree
* **Allways required**