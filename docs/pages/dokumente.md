# Dokumente page
----

* [Screens](#dokumente-screens)
* [Wizard](#dokumente-wizard)
* [Files grid](#files-grid)

## Dokumente screens

![](../img/screen/dokumente/wizard_1.1.png)

## Dokumente wizard

`[POST] NOT IMPLEMENTED` 

### Request

``` javascript
{
    firstName: String,
    lastName: String,
    gender: String,
    birthdate: Date,
    nationality: String,
    idCard: String,
    idCardValidUntil: Date,
    phoneNumber: String,
    source: String,
    sourceCustom: String
}
```

### Response

``` javascript
{
    firstName: String,
    lastName: String,
    gender: String,
    birthdate: Date,
    nationality: String,
    idCard: String,
    idCardValidUntil: Date,
    phoneNumber: String,
    source: String,
    sourceCustom: String
}
```

### Validation

``` javascript
{
    firstName: [
        'Required',
        'Maximum 100 characters'
    ],
    lastName: [
        'Required',
        'Maximum 100 characters'
    ],
    gender: [
        'Required'
    ],
    birthdate: [
        'Date < Today'
    ],
    nationality: [
        'Required'
    ],
    idCard: [
        'Required'
    ],
    idCardValidUntil: [
        'Date > Today'
    ],
    phoneNumber: [
        'Only numbers and "+"'
    ],
    source: [
        'Required'
    ],
    sourceCustom: [
        'Required if source',
        'Maximum 100 characters'
    ]
}
```

## Files

### Request

### Response

### Validation

