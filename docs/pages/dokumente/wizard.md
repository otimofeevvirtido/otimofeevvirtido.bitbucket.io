# Dokumente wizard
-----

## Forms

![](../../img/component/dokumente/wizard_1.png)
![](../../img/component/dokumente/wizard_2.png)
![](../../img/component/dokumente/wizard_3.png)
![](../../img/component/dokumente/wizard_4.png)

## API

`[POST] NONE`

Request:

```javascript
{
    firstName: String,
    lastName: String,
    gender: String,
    birthdate: Date,
    nationality: String,
    idCard: String,
    idCardValidUntil: Date,
    phoneNumber: String,
    source: String,
    sourceCustom: String
}
```

Response: 

```javascript
{}
```

# Validation

## First name
* Maximum 100 characters

## Last name
* Maximum 100 characters

## Birthday 
* Provided date must be < than today

## Nationality
* Only provided values available

## Gender
* Only provided values available

## Id card
* Only provided values available

## Id card expire
* Provided date must be > that today

## Tel number
* only characters and '+' allowed

## Source
* Only provided values available

## Custom source
* Max 100 characters

# Enum values

