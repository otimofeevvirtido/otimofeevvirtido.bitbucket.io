# Signup page
----

* [Screens](#screens)
* [Registration init](#registration-init)
* [Resend confirm email](#resend-confirm-email)
* [Confirm registration](#confirm-registration)

## Screens

### Init wizard

![](../img/screen/signup/init_1.png)

### Registration confirm wizard

![](../img/screen/signup/confirm_1.png)

## Registration init

`[POST] /api/registration` 

### Request:

``` javascript
{
    email: String,
    password: String,
    confirmPassword: String
}
```

### Response:

``` javascript
{
    email: Boolean,
    password: Boolean,
    email_sent: Boolean
}
```

### Validation:

```javascript 
{
    email: [
        'Only email style [part_1].[part_2].[domain]',
        'Maximum 100 characters'
    ],
    password: [
        'Minimum 8 characters',
        'Maximum 100 characters',
        'Minimum 1 number',
        'Minimum 1 UPPER CASE',
        'Minimum 1 lower case'
    ],
    confirmPassword: [
        'Match password field'
    ]
}

``` 

## Resend confirm email

`[POST] /api/registration/resend` 

### Requiest:

```javascript
{
    email: String
}
```

### Response:

``` javascript
{
    email: Boolean,
    password: Boolean,
    email_sent: Boolean
}
```

### Validation:

``` javascript
{
    email: [
        'Only email style [part_1].[part_2].[domain]',
        'Maximum 100 characters'
    ]
}
```

## Confirm registration

`[POST] NOT IMPLEMENTED` 

### Request:

```javascript 
{

    haveBudget: Boolean,
    docBudget: FileList,
    docCV: FileList,
    description: String,
    gdprAgree: Boolean

}

``` 

### Response: 

```javascript
{
    haveBudget: Boolean,
    docBudget: Boolean,
    docCV: Boolean,
    description: Boolean,
    gdprAgree: Boolean
}
```

### validation:

``` javascript
{
    haveBudget: [],
    docBudget: [
        'Required if haveBudget == true',
        'Extensions: .doc,.docx,.xml,.xls,.xlsx',
        'Formats: application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf'
    ],
    docCV: [
        'Required if haveBudget == true',
        'Extensions: .doc,.docx,.xml,.xls,.xlsx',
        'Formats: application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf'
    ],
    description: [
        'Required if haveBudget == true',
        'Minimum 100 characters',
        'Maximum 600 characters'
    ],
    gdprAgree: [
        'Always required'
    ]
}
```

