# Dashboard page
----
## First variation
![](../../img/screen/dashboard/dashboard_1.1.png)
![](../../img/screen/dashboard/dashboard_1.2.png)

## Second variation
![](../../img/screen/dashboard/dashboard_2.1.png)
![](../../img/screen/dashboard/dashboard_2.2.png)
